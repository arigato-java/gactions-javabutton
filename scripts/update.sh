#!/bin/sh
set -e
REPO_ROOT="$(dirname $0)/.."
ACTION_JSON="$REPO_ROOT/assets/action.json"
jq . "$ACTION_JSON"
gactions update --project JavaButtonAction --action_package "$ACTION_JSON"

