'use strict';

const {actionssdk} = require('actions-on-google');
const functions = require('firebase-functions');

const app = actionssdk({debug: true});

app.intent('actions.intent.MAIN', (conv) => {
  conv.ask('あなたとジャバ! 格言を言って、というとジャバに関する豆知識を言います。 どうしますか?');
});

function closeWithKakugen(conv){
    const tbl=require('./kakugen.json');
    const k_id=Math.floor(Math.random()*tbl.length);
    let kakugen=tbl[k_id].t;
    return conv.close(kakugen);
}

app.intent('actions.intent.TEXT',(conv,text)=>{
    return closeWithKakugen(conv);
});

app.intent('download.arigato-java.KAKUGEN',(conv)=>{
    return closeWithKakugen(conv);
});

exports.myFunction = functions.https.onRequest(app);
